package vn.fit.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import vn.fit.entity.Student;
import vn.fit.service.StudentService;

@Controller
public class StudentController {
	@Autowired
	private StudentService studentService;

	@GetMapping("")
	public String index(Model model) {
		model.addAttribute("students", studentService.getList());
		return "studentManager";
	}

	@GetMapping("/studentDetail")
	public String studentDetail(Model model, @RequestParam("id") String id) {
		model.addAttribute("student", studentService.getById(id));
		return "studentDetail";
	}
	
	@GetMapping("/showFormNew")
	public String showFormNew(Model model) {
		Student student = new Student();
		model.addAttribute("student", student);
		return "newStudent";
	}

	@RequestMapping(value = "/newStudent", method = RequestMethod.POST)
	public String newStudent(@ModelAttribute Student student) {
		studentService.create(student);
		return "redirect:/";
	}

	@GetMapping("/showFormEdit")
	public String showFormEdit(Model model, @RequestParam("id") String id) {
		model.addAttribute("student", studentService.getById(id));
		return "editStudent";
	}

	@RequestMapping(value = "/editStudent", method = RequestMethod.POST)
	public String editStudent(@ModelAttribute Student student) {
		studentService.update(student.getId(), student);
		return "redirect:/";
	}
	
	@RequestMapping(value = "/deleteStudent", method = RequestMethod.POST)
	public String deleteStudent(@RequestBody String[] checkedList) {
		studentService.deleteSelectedById(checkedList);
		return "redirect:/";
	}
}
