package vn.fit.service.impl;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.fit.entity.Student;
import vn.fit.repository.StudentRepository;
import vn.fit.service.StudentService;

@Service
public class StudentServiceImpl implements StudentService {

	@Autowired
	private StudentRepository studentRepository;

	@Override
	public List<Student> getList() {
		return studentRepository.findAll();
	}

	@Override
	public Student getById(String id) {
		return studentRepository.findById(id).get();
	}

	@Override
	public Student create(Student student) {
		return studentRepository.save(student);
	}

	@Override
	public Student update(String id, Student student) {
		Student localStudent = studentRepository.findById(id).get();
		try {
			if (localStudent != null) {
				localStudent.setName(student.getName());
				localStudent.setMale(student.isMale());
				localStudent.setBirthday(student.getBirthday());
				localStudent.setPlaceOfBirth(student.getPlaceOfBirth());
				localStudent.setAddress(student.getAddress());
				localStudent.setDepName(student.getDepName());

				localStudent = studentRepository.save(localStudent);
			} else {
				throw new RuntimeException("Student has id = " + id + " doesn't exist! Nothing will be done!");
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return localStudent;
	}

	@Override
	public void deleteById(String id) {
		studentRepository.deleteById(id);
	}

	@Override
	public void deleteSelectedById(String[] ids) {
		studentRepository.deleteStudentsWithIds(Arrays.asList(ids));
		
	}

}
