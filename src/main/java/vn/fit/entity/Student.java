package vn.fit.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
public class Student {

	@Id
//	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "studentid", columnDefinition = "CHAR(8)")
	private String id;

	@Column(columnDefinition = "VARCHAR(30)", nullable = false)
	private String name;

	@Column(columnDefinition = "BIT DEFAULT 1")
	private boolean male;

	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date birthday;

	@Column(columnDefinition = "VARCHAR(25)")
	private String placeOfBirth;

	@Column(columnDefinition = "VARCHAR(30)")
	private String address;

	@Column(columnDefinition = "VARCHAR(20)")
	private String depName;

	public Student() {
	}

	public Student(String id, String name, boolean male, Date birthday, String placeOfBirth, String address,
			String depName) {
		this.id = id;
		this.name = name;
		this.male = male;
		this.birthday = birthday;
		this.placeOfBirth = placeOfBirth;
		this.address = address;
		this.depName = depName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isMale() {
		return male;
	}

	public void setMale(boolean male) {
		this.male = male;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getPlaceOfBirth() {
		return placeOfBirth;
	}

	public void setPlaceOfBirth(String placeOfBirth) {
		this.placeOfBirth = placeOfBirth;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getDepName() {
		return depName;
	}

	public void setDepName(String depName) {
		this.depName = depName;
	}

}
